import numpy as np
from matplotlib import pyplot as plt 


############################################################################################################
# @brief: Gillespie simulator for stochastic Petri nets														#
# @input:	S:  	Stoichiometry matrix. Two-dimenstional numpy array with size N x V, where				#
#					N is the number of species and V is the number of reactions								#
#			M:  	Initial state vector. One-dimensional numpy array with length N 						#
#			h: 		Function that evaluates all reaction hazards and returns them as a 						#
#					one-dimensional numpy array of lenght V 												#
#			c:		Vector of stochastic rate constants. One-dimensional numpy array  						#
#					with length V 																			#
#			t_max:	Simulation time span length 															#
# @output:	T:		One-dimensional numpy array, containing the reaction event times 						#
#			X:		Two-dimensional numpy array, where the rows contain system state at each event time 	#
#############################################################################################################
def gillespie(S, M, h, c, t_max):
	N = S.shape[0]
	V = S.shape[1]
	max_reactions = 1000000
	T = np.zeros(max_reactions)
	X = np.zeros((max_reactions, N))
	current_state = M
	current_time = 0.0
	flag = 0
	# Main loop
	for index in range(0, max_reactions):
	    	    
	    # Record current state and time
		T[index]    = current_time
		X[index, :] = current_state
		if sum(current_state)==0:
			print("Both species went extinct at t = " + str(round(current_time,2)) + ".")
			break

	    # Compute current reaction hazards
		rates = h(current_state, c)

	    # EXERCISE a): SAMPLE THE NEXT REACTION EVENT TIME
		current_time += np.random.exponential(1/sum(rates))

		if (current_time > 2000 and flag == 0):
			flag = 1
			current_state[7] = current_state[7] + 10000
			print("Add 10000")

	    # Are we done?
		if current_time > t_max:
			break

	    # EXERCISE b): SAMPLE THE INDEX OF THE NEXT REACTION
		p = rates/sum(rates) # reaction probabilities
		idx_reaction = np.random.choice(len(p), size = 1, p = p)


		# Update current state
		current_state = current_state + S[:, idx_reaction].flatten()

	# Trim the output vectors
	T = T[0:index];
	X = X[0:index, :];

	return T, X

#############################################################################################################
# @brief: Poisson approximation method	(Naive version) 													#
# @input:	S:  	Stoichiometry matrix. Two-dimenstional numpy array with size N x V, where				#
#					N is the number of species and V is the number of reactions								#
#			M:  	Initial state vector. One-dimensional numpy array with length N 						#
#			h: 		Function that evaluates all reaction hazards and returns them as a 						#
#					one-dimensional numpy array of lenght V 												#
#			c:		Vector of stochastic rate constants. One-dimensional numpy array  						#
#					with length V 																			#
#			T:	Simulation time span length 																#
# @output:	T:		One-dimensional numpy array, containing the reaction event times 						#
#			X:		Two-dimensional numpy array, where the rows contain system state at each event time 	#
#############################################################################################################
def poisson(S, M, h, c, T):
	N = S.shape[0]
	V = S.shape[1]
	X = np.zeros((len(T), N))
	current_state = np.copy(M)
	current_time = 0.0
	X[0, :] = current_state
	flag = 0

	# Main loop
	for idx in range(1, len(T)):
		if (current_time > 2000 and flag == 0):
			flag = 1
			current_state[7] = current_state[7] + 10000
			print("Add 10000")

		# Compute current reaction hazards
		rates = h(current_state, c);

		# Compute Delta t
		Delta_t = T[idx] - T[idx - 1]
		current_time += Delta_t

		# Sample Poisson random numbers
		r = np.random.poisson(rates * Delta_t)

		# Update the current state
		current_state += S @ r

		#  Truncate to zero
		current_state[current_state < 0] = 0

		# Record current state
		X[idx, :] = current_state

	return T, X

#############################################################################################################
# @brief: Poisson approximation method 	(Tau leaping version)												#
# @input:	S:  		Stoichiometry matrix. Two-dimenstional numpy array with size N x V, where			#
#						N is the number of species and V is the number of reactions							#
#			M:  		Initial state vector. One-dimensional numpy array with length N 					#
#			h: 			Function that evaluates all reaction hazards and returns them as a 					#
#						one-dimensional numpy array of lenght V 											#
#			c:			Vector of stochastic rate constants. One-dimensional numpy array  					#
#						with length V 																		#
#			t_max:		Simulation time span lenght 														#
#			e:			Epsilon																				#
#			g_vector:	prescribed Function of state Xi(t)													#
# @output:	T:		One-dimensional numpy array, containing the reaction event times 						#
#			X:		Two-dimensional numpy array, where the rows contain system state at each event time 	#
#############################################################################################################
def tau_poisson(S, M, h, c, t_max, epsilon, g_vector):
	N = S.shape[0]
	V = S.shape[1]
	max_arr = 10000000
	T = np.zeros(max_arr)
	X = np.zeros((max_arr, N))
	current_state = M
	current_time = 0.0
	flag = 0

	for index in range(0, max_arr):
		# Compute current reaction hazards
		rates = h(current_state, c)

		# Calcuate dt
		mu = np.dot(S,rates)
		var = np.dot(S*S, rates)

		options = np.zeros(N)

		part = np.divide(current_state.ravel(), g_vector)*epsilon		# eps*x[i]/g[i] for all i
		num = np.array([part,np.ones(len(part))])                  		# eps*x[i]/g[i] for all i , 1 for all i
		numerator = num.max(axis=0)                                		# max(eps*x[i]/g[i],1) for all i
		abs_mu = np.abs(mu)										   		# abs(mu) for all i
		bound1 = np.divide(numerator,abs_mu)                       		# max(eps*x[i]/g[i],1) / abs(mu[i]) for all i
		numerator_square = np.square(numerator)
		bound2 = np.divide(numerator_square,var)         		  		# max(eps*x[i]/g[i],1)^2 / abs(mu[i]) for all i
		tau_primes = np.array([bound1,bound2])

		try:
			dt = np.min(tau_primes[~np.isnan(tau_primes)]) 				# min (bound1,bound2)
		except:
			dt = 10**6

		#print(dt)
		# Are we done?
		current_time += dt

		if (current_time > 2000 and flag == 0):
			flag = 1
			current_state[7] = current_state[7] + 10000
			print("Add 10000")

		if current_time > t_max:
			break

		# Sample Poisson random numbers
		r = np.random.poisson(rates * dt)

		# Update the current state
		current_state += S @ r

		#  Truncate to zero
		current_state[current_state < 0] = 0

		# Record current state and time
		T[index]    = current_time
		X[index, :] = current_state

	# Trim the output vectors
	T = T[0:index]
	X = X[0:index, :]

	return T, X

#############################################################################################################
# @brief: 			Diffusion process approximation/ Chemical Langein Method (naive version)				#
# @input:	S:  	Stoichiometry matrix. Two-dimenstional numpy array with size N x V, where				#
#					N is the number of species and V is the number of reactions								#
#			M:  	Initial state vector. One-dimensional numpy array with length N 						#
#			h: 		Function that evaluates all reaction hazards and returns them as a 						#
#					one-dimensional numpy array of lenght V 												#
#			c:		Vector of stochastic rate constants. One-dimensional numpy array  						#
#					with length V 																			#
#			T:	Simulation time span length 																#
# @output:	T:		One-dimensional numpy array, containing the reaction event times 						#
#			X:		Two-dimensional numpy array, where the rows contain system state at each event time 	#
#############################################################################################################
def CLE(S, M, h, c, T):
	N = S.shape[0]
	V = S.shape[1]
	X = np.zeros((len(T), N))
	current_state = np.asarray(M, dtype=np.float64)
	current_time = 0.0
	X[0, :] = current_state
	flag = 0

	# Main loop
	for idx in range(1, len(T)):

		# Compute current reaction hazards
		rates = h(current_state, c)

		# Compute Delta t
		Delta_t = T[idx] - T[idx - 1]
		current_time += Delta_t

		 # Sample Delta W
		Delta_W = np.sqrt(Delta_t) * np.random.randn(V)

		# Update the current state
		current_state += S @ (rates * Delta_t + np.sqrt(rates) * Delta_W)

		# Round to integers and truncate to zero
		current_state[current_state < 0] = 0

		if (current_time > 2000 and flag == 0):
			flag = 1
			current_state[7] = current_state[7] + 10000
			print("Add 10000")

		# Record current state rounded to an integer
		X[idx, :] = np.round(current_state)

	return T, X

#############################################################################################################
# @brief: 			Diffusion process approximation/ Chemical Langein Method (tau leaing version)			#
# @input:	S:  	Stoichiometry matrix. Two-dimenstional numpy array with size N x V, where				#
#					N is the number of species and V is the number of reactions								#
#			M:  	Initial state vector. One-dimensional numpy array with length N 						#
#			h: 		Function that evaluates all reaction hazards and returns them as a 						#
#					one-dimensional numpy array of lenght V 												#
#			c:		Vector of stochastic rate constants. One-dimensional numpy array  						#
#					with length V 																			#
#			t_max:		Simulation time span lenght 														#
#			e:			Epsilon																				#
#			g_vector:	prescribed Function of state Xi(t)													#
# @output:	T:		One-dimensional numpy array, containing the reaction event times 						#
#			X:		Two-dimensional numpy array, where the rows contain system state at each event time 	#
#############################################################################################################
def tau_CLE(S, M, h, c, t_max, epsilon, g_vector):
	N = S.shape[0]
	V = S.shape[1]
	max_arr = 10000000
	T = np.zeros(max_arr)
	X = np.zeros((max_arr, N))
	current_state = np.asarray(M, dtype=np.float64)
	X[0, :] = current_state
	current_time = 0.0

	for index in range(0, max_arr):
		# Compute current reaction hazards
		rates = h(current_state, c)

		# Calcuate dt
		mu = np.dot(S,rates)
		var = np.dot(S*S, rates)

		options = np.zeros(N)

		part = np.divide(current_state.ravel(), g_vector)*epsilon		# eps*x[i]/g[i] for all i
		num = np.array([part,np.ones(len(part))])                  		# eps*x[i]/g[i] for all i , 1 for all i
		numerator = num.max(axis=0)                                		# max(eps*x[i]/g[i],1) for all i
		abs_mu = np.abs(mu)										   		# abs(mu) for all i
		bound1 = np.divide(numerator,abs_mu)                       		# max(eps*x[i]/g[i],1) / abs(mu[i]) for all i
		numerator_square = np.square(numerator)
		bound2 = np.divide(numerator_square,var)         		  		# max(eps*x[i]/g[i],1)^2 / abs(mu[i]) for all i
		tau_primes = np.array([bound1,bound2])

		try:
			dt = np.min(tau_primes[~np.isnan(tau_primes)]) 				# min (bound1,bound2)
		except:
			dt = 10**6

		#print(dt)
		# Are we done?
		current_time += dt
		if current_time > t_max:
			break

		 # Sample Delta W
		Delta_W = np.sqrt(dt) * np.random.randn(V)

		# Update the current state		
		current_state += S @ (rates * dt + np.sqrt(rates) * Delta_W)

		#  Truncate to zero
		current_state[current_state < 0] = 0

		# Record current state and time
		T[index]    = current_time
		X[index, :] = np.round(current_state)

	# Trim the output vectors
	T = T[0:index]
	X = X[0:index, :]

	return T, X

#############################################################################################################
# @brief: 			Forward Euler Method																	#
# @input:	ODEfun	:	ODE function																		#			
#			x0		: 	initial state 																		#
#			dt 		: 	time delta 																			#
#			t_max	: 	Simulation time span length															#
# @output:	X_list	: 	Two-dimensional numpy array, where the rows contain system state 					#
#############################################################################################################
def fwEuler(ODEfun, x0, dt, t_max):
	t = 0
	X_list = [x0]
	X = x0

	while (t < t_max):
		X = X + dt*ODEfun(X)
		t = t + dt
		X_list.append(X)

	return np.array(X_list)

#########################################
# PLOT FUNCTION 						#
#########################################
def plot_result(T, X, ob, til):
	plt.figure(figsize = (10,6))
	plt.plot(T, X);
	plt.xlabel('Time')
	plt.ylabel('Concentration')
	plt.legend(ob)
	plt.title(til)
	plt.show()

def sub_plot_result(T,X, ob, til):
	fig = plt.figure()

	for i in range(len(ob)):
		plt.subplot(len(ob), 1, i+1)
		plt.plot(T, X[:,i])
		plt.xlabel("Time")
		plt.ylabel(ob[i])

	fig.suptitle(til)
	plt.show()