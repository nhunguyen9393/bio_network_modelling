#############################
# 1. Dimerisation kinetics  #
#############################
import numpy as np 
from matplotlib import pyplot as plt
import lib
import time


method1 = "Gilespie SSA"
method2 = "Poisson approximation"
method3 = "Chemical Lagevin"
method4 = "Deterministic simulation"

def hazard(x, c):	
	return c*np.array([x[0]*(x[0]-1)/2, x[1]])


def odefun(x):
    k = [5*pow(10,5), 0.2]
    
    f1 = -2*k[0]*pow(x[0], 2) + 2*k[1]*x[1]
    f2 = k[0]*pow(x[0], 2) - k[1]*x[1]
    
    return np.array([f1, f2])

M = np.array([301, 2])
c = np.array([0.00166, 0.2])
S = np.array([[-2, 2],  [1, -1]])
objects =  ("Protein P", "Protein P2")
Nt = 1000
t_max = 30
run_time = 100
gil_rt = 0
poi_rt = 0
cle_rt = 0
det_rt = 0
######################################################
# Run all algorithm(s)								 #	
######################################################
# Gillipsie
min_size = 100000
total_X = np.zeros((min_size, S.shape[0]))
T = np.zeros(min_size)
a = tuple()


for i in range(run_time):
	start_time = time.time()
	[T1,X1] = lib.gillespie(S, M, hazard, c, t_max)
	gil_rt += (time.time() - start_time)
	a = a + tuple([X1])

	if (len(T1) < min_size):
		min_size = len(T1)
		T = T1

P = np.zeros((min_size, run_time ))
P2 = np.zeros((min_size, run_time ))


for y in range(min_size): # Time
	for x in range(run_time ):
		P[y][x] = a[x][y][0]
		P2[y][x] = a[x][y][1]


avg_X = np.transpose(np.array([P.mean(axis=1), P2.mean(axis=1)]))
std_X = np.transpose(np.array([P.std(axis=1), P2.std(axis=1)]))
minus_X = avg_X - std_X
add_X = avg_X + std_X


plt.figure(figsize = (10,6))
plt.plot(T,avg_X)
plt.legend(("Protein P", "Protein P2"))
plt.plot(T,minus_X, 'k--')
plt.plot(T, add_X, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title(method1)
plt.show()


# Poisson
a = []
for i in range(run_time ):
	start_time = time.time()
	T, X1 = lib.poisson(S, M, hazard, c, np.linspace(0,t_max,Nt))
	poi_rt += (time.time() - start_time)
	a.append(X1)

P = np.transpose(a)[0]
P2 = np.transpose(a)[1]

avg_X = np.transpose(np.array([P.mean(axis=1), P2.mean(axis=1)]))
std_X = np.transpose(np.array([P.std(axis=1), P2.std(axis=1)]))
minus_X = avg_X - std_X
add_X = avg_X + std_X


plt.figure(figsize = (10,6))
plt.plot(T,avg_X)
plt.legend(("Protein P", "Protein P2"))
plt.plot(T,minus_X, 'k--')
plt.plot(T, add_X, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title(method2)
plt.show()

# CLE

a = []
for i in range(run_time):
	start_time = time.time()
	T, X1 = lib.CLE(S, M, hazard, c, np.linspace(0, t_max, Nt))
	cle_rt += (time.time() - start_time)
	a.append(X1)

P = np.transpose(a)[0]
P2 = np.transpose(a)[1]

avg_X = np.transpose(np.array([P.mean(axis=1), P2.mean(axis=1)]))
std_X = np.transpose(np.array([P.std(axis=1), P2.std(axis=1)]))
minus_X = avg_X - std_X
add_X = avg_X + std_X


plt.figure(figsize = (10,6))
plt.plot(T,avg_X)
plt.legend(("Protein P", "Protein P2"))
plt.plot(T,minus_X, 'k--')
plt.plot(T, add_X, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title(method3)
plt.show()


# Forward ODE
x0 = np.transpose([5*pow(10,-7), 0])     
dt = 0.03

start_time = time.time()
X1d = lib.fwEuler(odefun, x0, dt, t_max)
det_rt = time.time() - start_time
t_plot = np.linspace(0, t_max, int(t_max/dt))
lib.plot_result(t_plot, X1d[1:len(t_plot)+1,:], objects, method4)


#############################################
# PRINT OUT RUNNING TIME of EACH ALGORITHM 	#
#############################################
print("Gillipsie:     %s seconds" %str((gil_rt/run_time)))
print("Poisson: 	  %s seconds" %str((poi_rt/run_time)))
print("CLE: 		  %s seconds" %str((cle_rt/run_time)))
print("Deterministic: %s seconds" %str(det_rt))