###################
# 4. Lac operon	  #
###################
import numpy as np 
from matplotlib import pyplot as plt
import lib
import time

method1 = "Gilespie SSA"
method2 = "Poisson approximation"
method3 = "Chemical Lagevin"
method4 = "Deterministic simulation"

def hazard(x, c):
	h1 = x[0]
	h2 = x[1]
	h3 = x[2]*x[7]
	h4 = x[8]
	h5 = x[2]*x[3]
	h6 = x[9]
	h7 = x[3]*x[4]
	h8 = x[10]
	h9 = x[10]
	h10 = x[5]
	h11 = x[7]*x[6]
	h12 = x[1]
	h13 = x[2]
	h14 = x[8]
	h15 = x[5]
	h16 = x[6]

	H = np.array([h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h13, h14, h15, h16])

	return c*H

def odefun(x):
    N_A = 6.02*pow(10, 23)
    V = pow(10, -15)

    c = np.array([0.02, 0.1, 0.005, 0.1, 1, 0.01, 0.1, 0.01, 0.03, 0.1, pow(10, -5), 0.01, 0.002, 0.002, 0.01, 0.001])
    k = c.copy()
    k[2] = k[2]*N_A*V
    k[4] = k[4]*N_A*V
    k[6] = k[6]*N_A*V
    k[10] = k[10]*N_A*V    
    
    r1 = [0,1,0,0,0, 0,0,0,0,0, 0]
    r2 = [0,0,1,0,0, 0,0,0,0,0, 0]
    r3 = [0,0,-1,0,0, 0,0,-1,1,0, 0]
    r4 = [0,0,1,0,0, 0,0,1,-1,0, 0]
    r5 = [0,0,-1,-1,0, 0,0,0,0,1, 0]
    r6 = [0,0,1,1,0, 0,0,0,0,-1, 0]
    r7 = [0,0,0,-1,-1, 0,0,0,0,0, 1]
    r8 = [0,0,0,1,1, 0,0,0,0,0, -1]
    r9 = [0,0,0,1,1, 1,0,0,0,0, -1]
    r10 = [0,0,0,0,0, 0,1,0,0,0, 0]
    r11 = [0,0,0,0,0, 0,0,-1,0,0, 0]
    r12 = [0,-1,0,0,0, 0,0,0,0,0, 0]
    r13 = [0,0,-1,0,0, 0,0,0,0,0, 0]
    r14 = [0,0,0,0,0, 0,0,1,-1,0, 0]
    r15 = [0,0,0,0,0, -1,0,0,0,0, 0]
    r16 = [0,0,0,0,0, 0,-1,0,0,0, 0]

    A = np.array([r1, r2, r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14, r15, r16]).T
    
    B = np.array([k[0]*x[0], k[1]*x[1], k[2]*x[2]*x[7], k[3]*x[8], k[4]*x[2]*x[3],
                  k[5]*x[9], k[6]*x[3]*x[4], k[7]*x[10], k[8]*x[10], k[9]*x[5], 
                  k[10]*x[6]*x[7], k[11]*x[1], k[12]*x[2], k[13]*x[8], k[14]*x[5], k[15]*x[6]])

    return A.dot(B)


Pre = np.array([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
				[0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
				[0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
				[0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0],
				[0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
				[0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]])

Post = np.array([[1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
				[0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
				[0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
				[0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])

S = np.transpose(Post - Pre)
M = np.array([1, 0, 50, 1, 100, 0, 0, 20, 0, 0, 0])
g_vector = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
c = np.array([0.02, 0.1, 0.005, 0.1, 1, 0.01, 0.1, 0.01, 0.03, 0.1, 0.00001, 0.01, 0.002, 0.002, 0.01, 0.001])
objects = ( "Rna", "Z", "Lactose")
epsilon  = 0.3

t_max = 5000
Nt = 200000
run_time = 100
gil_rt = 0
poi_rt = 0
cle_rt = 0
det_rt = 0

######################################################
# Run all algorithm(s)								 #	
######################################################

min_size = 100000
total_X = np.zeros((min_size, S.shape[0]))
T = np.zeros(min_size)
a = tuple()

for i in range(run_time):
	print(i)
	start_time = time.time()
	[T3, X3] = lib.gillespie(S, M, hazard, c, t_max)
	gil_rt += time.time() - start_time

	X = X3[:,5:8]
	a = a + tuple([X])
	if (len(T3) < min_size):
		min_size = len(T3)
		T = T3

RNA = np.zeros((min_size, run_time))
Z 	= np.zeros((min_size, run_time))
Lac = np.zeros((min_size, run_time))

for y in range(min_size): # Time
	for x in range(run_time):
		RNA[y][x] 	= a[x][y][0]
		Z[y][x] 	= a[x][y][1]
		Lac[y][x] 	= a[x][y][2]

R_avg 	= RNA.mean(axis=1)
Z_avg 	= Z.mean(axis=1)
L_avg 	= Lac.mean(axis=1)

R_std 	= RNA.std(axis=1)
Z_std 	= Z.std(axis=1)
L_std 	= Lac.std(axis=1)

plt.figure(figsize = (10,4))
plt.plot(T, R_avg)
plt.plot(T, R_avg - R_std, 'k--')
plt.plot(T, R_avg + R_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("RNA - " + method1)


plt.figure(figsize = (10,4))
plt.plot(T, Z_avg)
plt.plot(T, Z_avg - Z_std, 'k--')
plt.plot(T, Z_avg + Z_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("Z - " + method1)


plt.figure(figsize = (10,4))
plt.plot(T, L_avg)
plt.plot(T, L_avg - L_std, 'k--')
plt.plot(T, L_avg + L_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("Lactose - " + method1)
plt.show()


## Poisson

a = []
for i in range(run_time):
	print(i)
	start_time = time.time()
	T, X3 = lib.poisson(S, M, hazard, c, np.linspace(0,t_max,Nt))
	poi_rt += time.time() - start_time
	# Get only RNA, P and P2
	X = X3[:,5:8]
	a.append(X)

RNA	= np.transpose(a)[0]
Z 	= np.transpose(a)[1]
Lac	= np.transpose(a)[2]

R_avg 	= RNA.mean(axis=1)
Z_avg 	= Z.mean(axis=1)
L_avg 	= Lac.mean(axis=1)

R_std 	= RNA.std(axis=1)
Z_std 	= Z.std(axis=1)
L_std 	= Lac.std(axis=1)


plt.figure(figsize = (10,4))
plt.plot(T, R_avg)
plt.plot(T, R_avg - R_std, 'k--')
plt.plot(T, R_avg + R_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("RNA - " + method2)


plt.figure(figsize = (10,4))
plt.plot(T, Z_avg)
plt.plot(T, Z_avg - Z_std, 'k--')
plt.plot(T, Z_avg + Z_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("Z - " + method2)


plt.figure(figsize = (10,4))
plt.plot(T, L_avg)
plt.plot(T, L_avg - L_std, 'k--')
plt.plot(T, L_avg + L_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("Lactose - " + method2)
plt.show()


## CLE

a = []
for i in range(run_time):
	print(i)
	start_time = time.time()
	T, X3 = lib.CLE(S, M, hazard, c, np.linspace(0,t_max,Nt))
	cle_rt += time.time() - start_time
	# Get only RNA, P and P2
	X = X3[:,5:8]
	a.append(X)

RNA	= np.transpose(a)[0]
Z 	= np.transpose(a)[1]
Lac	= np.transpose(a)[2]

R_avg 	= RNA.mean(axis=1)
Z_avg 	= Z.mean(axis=1)
L_avg 	= Lac.mean(axis=1)

R_std 	= RNA.std(axis=1)
Z_std 	= Z.std(axis=1)
L_std 	= Lac.std(axis=1)

plt.figure(figsize = (10,4))
plt.plot(T, R_avg)
plt.plot(T, R_avg - R_std, 'k--')
plt.plot(T, R_avg + R_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("RNA - " + method3)


plt.figure(figsize = (10,4))
plt.plot(T, Z_avg)
plt.plot(T, Z_avg - Z_std, 'k--')
plt.plot(T, Z_avg + Z_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("Z - " + method3)


plt.figure(figsize = (10,4))
plt.plot(T, L_avg)
plt.plot(T, L_avg - L_std, 'k--')
plt.plot(T, L_avg + L_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("Lactose - " + method3)
plt.show()


# Forward ODE
N_A = 6.02*pow(10, 23)
V = pow(10, -15)

names = ['Idna', 'Irna', 'I', 'Op', 'Rnap', 'Rna', 'Z', 'Lactose', 'ILactose', 'Iop', 'RnaOp']
x0disc = np.array([1, 0, 50, 1, 100, 0, 0, 20, 0, 0, 0])
x0 = x0disc/(N_A*V)
dt = 0.025
t_max = 2000  # simulations don't make much with t_max=200, and I didn't drive for 20 000, it will take long 
t_max2 = 3000

start_time = time.time()
X = lib.fwEuler(odefun, x0, dt, t_max)
x0new = X[-1, :].copy()  
x0new[7] = x0new[7] + 10000/(N_A*V)
X_ = lib.fwEuler(odefun, x0new, dt, t_max2)
det_rt += time.time() - start_time

X_whole = np.vstack((X, X_))
t_plot = np.linspace(0, (t_max+t_max2), int((t_max+t_max2)/dt))


plt.figure(figsize=(8, 2))
plt.plot(t_plot, X_whole[1:len(t_plot)+1, 7])
plt.title('Lactose - Forward ODE')

plt.figure(figsize=(8, 2))
plt.plot(t_plot, X_whole[1:len(t_plot)+1, 5])
plt.title('RNA - Forward ODE ')

plt.figure(figsize=(8, 2))
plt.plot(t_plot, X_whole[1:len(t_plot)+1, 6])
plt.title('Z - Forward ODE')
plt.show()


#############################################
# PRINT OUT RUNNING TIME of EACH ALGORITHM 	#
#############################################
print("Gillipsie:     %s seconds" %str((gil_rt/run_time)))
print("Poisson: 	  %s seconds" %str((poi_rt/run_time)))
print("CLE: 		  %s seconds" %str((cle_rt/run_time)))
print("Deterministic: %s seconds" %str(det_rt))