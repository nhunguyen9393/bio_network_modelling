##########################################
# 3. An auto-regulatory genetic network  #
##########################################
import numpy as np 
from matplotlib import pyplot as plt
import lib
import time

method1 = "Gilespie SSA"
method2 = "Poisson approximation"
method3 = "Chemical Lagevin"
method4 = "Deterministic simulation"

def hazard(x, c):
	h1 = int(x[0])*int(x[4])
	h2 = x[1]
	h3 = x[0]
	h4 = x[2]
	h5 = 0.5*int(x[3])*int((x[3]-1))
	h6 = x[4]
	h7 = x[2]
	h8 = x[3]

	return c*np.array([h1, h2, h3, h4, h5, h6, h7, h8])

def odefun(x):
    """Differential function f. 
    :param x: System state
    :returns: dx/dt 
    """
    N_A = 6.02*pow(10, 23)
    V = pow(10, -15)
    
    # k: Rate parameters
    k1 = 1*N_A*V
    k1r = 10
    k2 = 0.01
    k3 = 10
    k4 = 1*N_A*V/2
    k4r = 1
    k5 = 0.1
    k6 = 0.01
    
    g = x[0]
    gP2 = x[1]
    rna = x[2]
    P = x[3]
    P2 = x[4]
    
    dg_dt = -k1*g*P2 + k1r*gP2
    dgP2_dt = k1*g*P2 -k1r*gP2    
    drna_dt = k2*g-k5*rna
    dP_dt = k3*rna -2*k4*pow(P, 2) + 2*k4r*P2 -k6*P
    dP2_dt = -k1*g*P2+k1r*gP2 +k4*pow(P, 2) -k4r*P2

    return np.array([dg_dt, dgP2_dt, drna_dt, dP_dt, dP2_dt])


M 		 = np.array([10, 0, 0, 0, 0])
g_vector = np.array([1, 1, 1, 1, 1])
c 		 = np.array([1, 10, 0.01, 10, 1, 1, 0.1, 0.01])
S 		 = np.array([[-1 ,1, 0, 0, 0, 0, 0, 0],
				[1, -1, 0, 0, 0, 0, 0, 0],
				[0, 0, 1, 0, 0, 0, -1, 0],
				[0, 0, 0, 1, -2, 2, 0, -1],
				[-1, 1, 0, 0, 1, -1, 0, 0]])

objects3 = ("RNA", "P", "P2")
epsilon  = 0.3

t_max = 300
Nt = 200000
run_time = 100
gil_rt = 0
poi_rt = 0
cle_rt = 0
det_rt = 0

######################################################
# Run all algorithm(s)								 #	
######################################################

## Gillipsie
min_size = 1000000
total_X = np.zeros((min_size, S.shape[0]))
T = np.zeros(min_size)
a = tuple()

for i in range(run_time):
	print(i)
	start_time = time.time()
	[T3, X3] = lib.gillespie(S, M, hazard, c, t_max)
	gil_rt += time.time() - start_time

	# Get only RNA, P and P2
	X = X3[:,2:5]
	a = a + tuple([X])
	if (len(T3) < min_size):
		min_size = len(T3)
		T = T3

RNA = np.zeros((min_size, run_time))
P 	= np.zeros((min_size, run_time))
P2 	= np.zeros((min_size, run_time))

for y in range(min_size): # Time
	for x in range(run_time):
		RNA[y][x] 	= a[x][y][0]
		P[y][x] 	= a[x][y][1]
		P2[y][x] 	= a[x][y][2]

R_avg 	= RNA.mean(axis=1)
P_avg 	= P.mean(axis=1)
P2_avg 	= P2.mean(axis=1)

R_std 	= RNA.std(axis=1)
P_std 	= P.std(axis=1)
P2_std 	= P2.std(axis=1)


plt.figure(figsize = (10,4))
plt.plot(T, R_avg)
plt.plot(T, R_avg - R_std, 'k--')
plt.plot(T, R_avg + R_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("RNA - " + method1)


plt.figure(figsize = (10,4))
plt.plot(T, P_avg)
plt.plot(T, P_avg - P_std, 'k--')
plt.plot(T, P_avg + P_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("P - " + method1)


plt.figure(figsize = (10,4))
plt.plot(T, P2_avg)
plt.plot(T, P2_avg - P2_std, 'k--')
plt.plot(T, P2_avg + P2_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("P2 - " + method1)
plt.show()


## Poisson
a = []
for i in range(run_time):
	print(i)
	start_time = time.time()
	T, X3 = lib.poisson(S, M, hazard, c, np.linspace(0,t_max,Nt))
	poi_rt += time.time() - start_time
	# Get only RNA, P and P2
	X = X3[:,2:5]
	a.append(X)


RNA	= np.transpose(a)[0]
P 	= np.transpose(a)[1]
P2	= np.transpose(a)[2]

R_avg 	= RNA.mean(axis=1)
P_avg 	= P.mean(axis=1)
P2_avg 	= P2.mean(axis=1)

R_std 	= RNA.std(axis=1)
P_std 	= P.std(axis=1)
P2_std 	= P2.std(axis=1)


plt.figure(figsize = (10,4))
plt.plot(T, R_avg)
plt.plot(T, R_avg - R_std, 'k--')
plt.plot(T, R_avg + R_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title('RNA - ' + method2)


plt.figure(figsize = (10,4))
plt.plot(T, P_avg)
plt.plot(T, P_avg - P_std, 'k--')
plt.plot(T, P_avg + P_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title('P - ' + method2)


plt.figure(figsize = (10,4))
plt.plot(T, P2_avg)
plt.plot(T, P2_avg - P2_std, 'k--')
plt.plot(T, P2_avg + P2_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title('P2 - ' + method2)
plt.show()


## CLE
a = []
for i in range(run_time):
	print(i)
	start_time = time.time()
	T, X3 = lib.CLE(S, M, hazard, c, np.linspace(0,t_max,Nt))
	cle_rt += time.time() - start_time
	X = X3[:,2:5]
	a.append(X)


RNA	= np.transpose(a)[0]
P 	= np.transpose(a)[1]
P2	= np.transpose(a)[2]

R_avg 	= RNA.mean(axis=1)
P_avg 	= P.mean(axis=1)
P2_avg 	= P2.mean(axis=1)

R_std 	= RNA.std(axis=1)
P_std 	= P.std(axis=1)
P2_std 	= P2.std(axis=1)


plt.figure(figsize = (10,4))
plt.plot(T, R_avg)
plt.plot(T, R_avg - R_std, 'k--')
plt.plot(T, R_avg + R_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title('RNA - ' + method3)


plt.figure(figsize = (10,4))
plt.plot(T, P_avg)
plt.plot(T, P_avg - P_std, 'k--')
plt.plot(T, P_avg + P_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title('P - ' + method3)


plt.figure(figsize = (10,4))
plt.plot(T, P2_avg)
plt.plot(T, P2_avg - P2_std, 'k--')
plt.plot(T, P2_avg + P2_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title('P2 - ' + method3)
plt.show()

# Forward ODE
N_A = 6.02*pow(10, 23)
V = pow(10, -15)
x0 = np.transpose([10/(N_A*V), 0, 0, 0, 0])     
dt = 0.0015

start_time = time.time()
X = lib.fwEuler(odefun, x0, dt, t_max)
det_rt += time.time() - start_time
t_plot = np.linspace(0, t_max, int(t_max/dt))
X_plot = np.transpose([ X[1:len(t_plot)+1, 2], X[1:len(t_plot)+1, 3], X[1:len(t_plot)+1, 4]])
lib.sub_plot_result(t_plot, X_plot, objects3, method4)

#############################################
# PRINT OUT RUNNING TIME of EACH ALGORITHM 	#
#############################################
print("Gillipsie:     %s seconds" %str((gil_rt/run_time)))
print("Poisson: 	  %s seconds" %str((poi_rt/run_time)))
print("CLE: 		  %s seconds" %str((cle_rt/run_time)))
print("Deterministic: %s seconds" %str(det_rt))

'''
# Tau-leaping Poisson

min_size = 100000000
total_X = np.zeros((min_size, S.shape[0]))
T = np.zeros(min_size)
a = tuple()

for i in range(run_time):
	start_time = time.time()
	[T3, X3] = lib.tau_poisson(S, M, hazard, c, t_max, epsilon, g_vector)
	poi_rt += time.time() - start_time

	X = X3[:,2:5]
	a = a + tuple([X])
	if (len(T3) < min_size):
		min_size = len(T3)
		T = T3

RNA = np.zeros((min_size, run_time))
P 	= np.zeros((min_size, run_time))
P2 	= np.zeros((min_size, run_time))

for y in range(min_size): # Time
	for x in range(run_time):
		RNA[y][x] 	= a[x][y][0]
		P[y][x] 	= a[x][y][1]
		P2[y][x] 	= a[x][y][2]

R_avg 	= RNA.mean(axis=1)
P_avg 	= P.mean(axis=1)
P2_avg 	= P2.mean(axis=1)

R_std 	= RNA.std(axis=1)
P_std 	= P.std(axis=1)
P2_std 	= P2.std(axis=1)

plt.figure(figsize = (10,4))
plt.plot(T, R_avg)
plt.plot(T, R_avg - R_std, 'k--')
plt.plot(T, R_avg + R_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("RNA - " + method2)


plt.figure(figsize = (10,4))
plt.plot(T, P_avg)
plt.plot(T, P_avg - P_std, 'k--')
plt.plot(T, P_avg + P_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("P - " + method2)


plt.figure(figsize = (10,4))
plt.plot(T, P2_avg)
plt.plot(T, P2_avg - P2_std, 'k--')
plt.plot(T, P2_avg + P2_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("P2 - " + method2)
plt.show()



# Tau-leaping CLE
min_size = 100000000
total_X = np.zeros((min_size, S.shape[0]))
T = np.zeros(min_size)
a = tuple()

for i in range(run_time):
	start_time = time.time()
	[T3, X3] = lib.tau_CLE(S, M, hazard, c, t_max, epsilon, g_vector)
	cle_rt += time.time() - start_time

	X = X3[:,2:5]
	a = a + tuple([X])
	if (len(T3) < min_size):
		min_size = len(T3)
		T = T3

RNA = np.zeros((min_size, run_time))
P 	= np.zeros((min_size, run_time))
P2 	= np.zeros((min_size, run_time))

for y in range(min_size): # Time
	for x in range(run_time):
		RNA[y][x] 	= a[x][y][0]
		P[y][x] 	= a[x][y][1]
		P2[y][x] 	= a[x][y][2]

R_avg 	= RNA.mean(axis=1)
P_avg 	= P.mean(axis=1)
P2_avg 	= P2.mean(axis=1)

R_std 	= RNA.std(axis=1)
P_std 	= P.std(axis=1)
P2_std 	= P2.std(axis=1)

plt.figure(figsize = (10,4))
plt.plot(T, R_avg)
plt.plot(T, R_avg - R_std, 'k--')
plt.plot(T, R_avg + R_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("RNA - " + method3)


plt.figure(figsize = (10,4))
plt.plot(T, P_avg)
plt.plot(T, P_avg - P_std, 'k--')
plt.plot(T, P_avg + P_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("P - " + method3)


plt.figure(figsize = (10,4))
plt.plot(T, P2_avg)
plt.plot(T, P2_avg - P2_std, 'k--')
plt.plot(T, P2_avg + P2_std, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title("P2 - " + method3)
plt.show()
'''