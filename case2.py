########################################
# 2. Michaelis-Menten enzyme kinetics   #
########################################
import numpy as np 
from matplotlib import pyplot as plt
import lib
import time

method1 = "Gilespie SSA"
method2 = "Poisson approximation"
method3 = "Chemical Lagevin"
method4 = "Deterministic simulation"

def hazard(x, c):
	return c*np.array([x[0]*x[1], x[2], x[2]])

def odefun(x):
    k = [pow(10, 6), pow(10, -4), 0.1]

    dxdt = np.array([[-1, 1, 0], [-1, 1, 1], [1, -1, -1], [0, 0, 1]]).dot(np.array([k[0]*x[0]*x[1], k[1]*x[2], k[2]*x[2] ]).T)
    return dxdt

M = np.array([301, 120, 0, 0])
c = np.array([0.00166, 0.0001, 0.1])
S = np.array([[-1, 1, 0], [-1, 1, 1], [1, -1, -1], [0, 0, 1]])
objects = ("S", "E", "SE", "P")

Nt = 1000
t_max = 30
run_time = 100
gil_rt = 0
poi_rt = 0
cle_rt = 0
det_rt = 0

######################################################
# Run all algorithm(s)								 #	
######################################################

## Gillipsie
min_size = 100000
total_X = np.zeros((min_size, S.shape[0]))
T = np.zeros(min_size)
a = tuple()

for i in range(run_time):
	start_time = time.time()
	[T1,X1] = lib.gillespie(S, M, hazard, c, t_max)
	gil_rt += time.time() - start_time
	a = a + tuple([X1])

	if (len(T1) < min_size):
		min_size = len(T1)
		T = T1

S  = np.zeros((min_size, run_time))
E  = np.zeros((min_size, run_time))
SE = np.zeros((min_size, run_time))
P  = np.zeros((min_size, run_time))

for y in range(min_size): # Time
	for x in range(run_time):
		S[y][x] 	= a[x][y][0]
		E[y][x] 	= a[x][y][1]
		SE[y][x] 	= a[x][y][2]
		P[y][x] 	= a[x][y][3]


Avg = [S.mean(axis=1), E.mean(axis=1), SE.mean(axis=1), P.mean(axis=1)]
Std = [S.std(axis=1), E.std(axis=1), SE.std(axis=1), P.std(axis=1)]
avg_X = np.transpose(np.array(Avg))
std_X = np.transpose(np.array(Std))
minus_X = avg_X - std_X
add_X = avg_X + std_X


plt.figure(figsize = (10,6))
plt.plot(T,avg_X)
plt.legend(("S", "E", "SE", "P"))
plt.plot(T,minus_X, 'k--')
plt.plot(T, add_X, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title(method1)
plt.show()


## Poisson
M = np.array([301, 120, 0, 0])
c = np.array([0.00166, 0.0001, 0.1])
S = np.array([[-1, 1, 0], [-1, 1, 1], [1, -1, -1], [0, 0, 1]])

a = []
for i in range(run_time):
	start_time = time.time()
	T, X2 = lib.poisson(S, M, hazard, c, np.linspace(0,30,Nt))
	poi_rt += time.time() - start_time
	a.append(X2)


S  = np.transpose(a)[0]
E  = np.transpose(a)[1]
SE = np.transpose(a)[2]
P  = np.transpose(a)[3]

Avg = [S.mean(axis=1), E.mean(axis=1), SE.mean(axis=1), P.mean(axis=1)]
Std = [S.std(axis=1), E.std(axis=1), SE.std(axis=1), P.std(axis=1)]
avg_X = np.transpose(np.array(Avg))
std_X = np.transpose(np.array(Std))
minus_X = avg_X - std_X
add_X = avg_X + std_X


plt.figure(figsize = (10,6))
plt.plot(T,avg_X)
plt.legend(("S", "E", "SE", "P"))
plt.plot(T,minus_X, 'k--')
plt.plot(T, add_X, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title(method2)
plt.show()


## CLE
M = np.array([301, 120, 0, 0])
c = np.array([0.00166, 0.0001, 0.1])
S = np.array([[-1, 1, 0], [-1, 1, 1], [1, -1, -1], [0, 0, 1]])

a = []
for i in range(run_time ):
	start_time = time.time()
	T, X1 = lib.CLE(S, M, hazard, c, np.linspace(0, t_max, Nt))
	cle_rt += time.time() - start_time
	a.append(X1)

S  = np.transpose(a)[0]
E  = np.transpose(a)[1]
SE = np.transpose(a)[2]
P  = np.transpose(a)[3]

Avg = [S.mean(axis=1), E.mean(axis=1), SE.mean(axis=1), P.mean(axis=1)]
Std = [S.std(axis=1), E.std(axis=1), SE.std(axis=1), P.std(axis=1)]
avg_X = np.transpose(np.array(Avg))
std_X = np.transpose(np.array(Std))
minus_X = avg_X - std_X
add_X = avg_X + std_X

plt.figure(figsize = (10,6))
plt.plot(T,avg_X)
plt.legend(("S", "E", "SE", "P"))
plt.plot(T,minus_X, 'k--')
plt.plot(T, add_X, 'k--')
plt.xlabel("Time")
plt.ylabel("Number of individuals")
plt.title(method3)
plt.show()

# Forward ODE
x0 = np.transpose([5*pow(10, -7), 2*pow(10, -7), 0, 0])     
dt = 0.03
t_max = 30

start_time = time.time()
X2d = lib.fwEuler(odefun, x0, dt, 30)
det_rt = time.time() - start_time
t_plot = np.linspace(0, t_max, int(t_max/dt))
lib.plot_result(t_plot, X2d[1:len(t_plot)+1,:], objects, method4)


#############################################
# PRINT OUT RUNNING TIME of EACH ALGORITHM 	#
#############################################
print("Gillipsie:     %s seconds" %str((gil_rt/run_time)))
print("Poisson: 	  %s seconds" %str((poi_rt/run_time)))
print("CLE: 		  %s seconds" %str((cle_rt/run_time)))
print("Deterministic: %s seconds" %str(det_rt))